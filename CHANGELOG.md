## v0.2.0 (2024-04-16)

### Feat

- use poetry-plugin-mono-repo-deps

### Fix

- skip setup.py if not in the tar.gz
- include example post-checkout hook to keep the venvs in sync with the lock files
- to use as described in blogpost

## v0.1.0 (2022-03-17)

### Fix

- accidentally committed the replaced path dependencies
