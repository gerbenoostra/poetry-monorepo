# Poetry Monorepo
Demonstrates how to set up a Poetry mono-repo where
* you can have multiple Python poetry packages in one git repo
* the packages depend on each other using `--dev` **path** dependencies for local development
* built packages only use name & version dependencies
* packages within the repo can each be published to Pypi (wheel & tar.gz)
* docker files can be built from a branch (without having to release packages to a PyPi)
* conventional commits are used to determine SemVer version
* new SemVer versions are automatically released (as tag & Gitlab release)
* all Gitlab builds are done with local version identifiers
* you can easily run `poetry build`, `poetry lock`, `poetry lock --no-update` and `poetry install` on all inner projects

## Background
Poetry is a great tool to manage individual packages.
It however can't (out of the box) handle and manage mono-repos. 
There is a discussion regarding mono-repo's in [Python-Poetry](https://github.com/python-poetry/poetry/) issues [#936](https://github.com/python-poetry/poetry/issues/936) and [#2270](https://github.com/python-poetry/poetry/issues/2270).
In this example repo I'll show how one can (quite easily) utilize Poetry in a mono-repo.

Ideally, a full-fledged Poetry mono-repo would allow to:
* develop with path dependencies, while packaging with named versions 
* run commands on all contained packages 
* inherit configuration from a parent configuration
* add a dependency to one package (`poetry add`), that will update both the Poetry lock file of that package and the Poetry lock file of all other packages that (indirectly) depend on it.
* keep a consistent version number across all packages 

## Adapting Poetry to mono-repo's
I'm not the first one doing this, other examples are:
* [ya-mori/python-monorepo](https://github.com/ya-mori/python-monorepo), which shows how to have multiple libraries and projects in one repo. Dependencies are all 'editable installs' (path dependencies). Note that built artifacts still depend on path dependencies.
* [dermidgen](https://github.com/dermidgen/python-monorepo) uses Makefiles. Applications depend on packages by path dependencies in their `[tool.poetry.dev-dependencies]` section. The [makefile](https://github.com/dermidgen/python-monorepo/blob/master/tools/Packages.mk) extracts the package names and will build a wheel for each one. Those wheels, together with the application, will be installed in the docker container. Note that the built artifacts don't have the internal dependencies in their dependency list.
* [Python Polylith](https://davidvujic.github.io/python-polylith-docs/) solves the dependencies within the repo by including the sources of those dependencies within the build of each package. Thus if package A depends on B, and B depends on C, the build of package A will contain the sources of A, B and C, and the build of package B will contain the sources of both B and C.

My approach here is slightly different, in which I'll allow releasing the packages with valid named dependencies.

In this repository, I demonstrate how:
* to develop with path dependencies, while packaging with named versions
* simple bash scripts allow to run commands against all packages
* how to use Commitizen to update the version of all packages.
* how to use Dunamai to always version builds, even if not on a tag.


## Working with a Poetry mono-repo

### Creating virtual environments
As each mono-repo package can have a different set of dependencies, each needs its virtual environment.

To create the poetry virtual environments (for all packages):
```shell
scripts/poetry_install.sh
```

This will just run `poetry install --sync` on each contained package.

### Updating the lock files 
If the lock file isn't consistent with the `pyproject.toml` anymore, you can run `poetry lock --no-update` on all packages with:
```shell
scripts/poetry_lock_no_update.sh
```
This is also useful when you're running on new architectures.

If a new dependency has been added to one of the mono-repo packages, the other packages need their lock files to be updated.
Unfortunately, the `poetry lock --no-update` will not detect updated dependencies downstream, thus a `poetry lock` is needed, for all projects, in their (dependency graph's) topological order.

Run the following helper [script](https://gitlab.com/gerbenoostra/poetry-monorepo/-/blob/main/scripts/create_local_version.sh) to update all lock files:
```shell
scripts/poetry_update.sh
```
It will update all `poetry.lock` files (using `poetry lock`), such that updated transitive dependencies are correct.
As a side effect, it will also update other transitive dependencies to their latest versions.

### Bumping the version
The Gitlab pipeline contains a `bump version` step, which is run on every push to `main`. 
It uses [Commitizen](https://github.com/commitizen-tools/commitizen) to determine the new [SemVer](https://semver.org/) version, based on the latest tag and the commits since then.

If there's no version increment (for example because there are only `chore:` commits), it will have no effect.
Otherwise, it commits the updated version numbers, creates a corresponding tag, and pushes both to GitLab.

The new tag will then result in building the artifacts which can be released to a PyPi.

In this way, we automatically create new releases.

### Determining the local build version
When building artifacts, we first create a local SemVer version using .
This ensures we have the correct version for the branch (whether it is a tag, a specific commit, or one with uncommitted changes).

This is done with this [script](https://gitlab.com/gerbenoostra/poetry-monorepo/-/blob/main/scripts/create_local_version.sh):
```shell
poetry run scripts/create_local_version.sh
```
It will determine the valid version identifier of the repository as it is checked out. 
The version is determined using [Dunamai](https://github.com/mtkennerly/dunamai), which is then written to all files containing the version.
It will thus modify the root [VERSION](https://gitlab.com/gerbenoostra/poetry-monorepo/-/blob/main/VERSION) file, the `__version__` variables in the Python code, and the version in the `pyproject.toml` files.
These files are all listed explicitly in the bash script.

This is similar to Commitizen, but Commitizen will also commit the changes and create a tag. Therefore we list both the version files in Commitizen's configuration and our `create_local_version.sh` script.

Note that this changes the `pyproject.toml` files and all Python files containing versions, which **should not be committed** to git.

### Building artifacts with named dependencies

The codebase here
* Uses path dependencies, like `package-a = {path="../package-a", develop=true}`, that are checked into git.
* Built wheels will get a versioned dependency, like `package-a="^VERSION"`, where VERSION is the current version of the mono-repo. 

This can be done in three different ways:
* Temporarily modify the `pyproject.toml` (but not updating the `poetry.lock` file), and then use `poetry build` to create valid artifacts.
* Build the wheel & tar.gz artifacts normally, and afterward replace the path dependencies with name + version dependencies in the artifacts.
* Use the [poetry-mono-repo-deps](https://github.com/gerbenoostra/poetry-plugin-mono-repo-deps/) Poetry plugin. It alters Poetry's Build (and Export) command such that it interprets the path dependencies as name + version dependencies.

As a result, the wheel will contain only **named** dependencis, even though development is done using **path** dependencies. Combining the best of both worlds.

**Building with temporary pyproject files**

This approach can be done by running the following [script](https://gitlab.com/gerbenoostra/poetry-monorepo/-/blob/main/scripts/poetry_build.sh):
```shell
scripts/poetry_build.sh
```
Note that this temporarily changes the `pyproject.toml` files with changes that **should not be committed** to git.

The `poetry_build.sh` works as follows:
* It scans the `pyproject.toml` for path dependencies like `package-a = {path="../package-a", develop=true}`
* These are then replaced into `package-a="^VERSION"`, where `VERSION` is the current version of the mono-repo
* Then it builds the Python package using `poetry build`. As a result, the wheel will contain a **named** dependency, even though development is done using **path** dependencies.
* All wheels are then collected to the `dist/` folders of each package (the root and every package). This allows us to build the docker files.

The main downside of this approach is that the `pyproject.toml` is updated before building.
That change **should not be committed**, as it is only aimed at having Poetry create a wheel with the correct dependencies.

**Modifying the built artifacts**

Instead of updating the `pyproject.toml`, and relying on `poetry` to create a correct wheel, we can also fix the wheel & `tar.gz` artifacts **after** building with `poetry`.

This is done as follows (without invoking `poetry_build.sh`):
```shell
cd package-b
poetry build
../scripts/replace_path_deps.sh
```

The `scripts/replace_path_deps.sh` works as follows:
* It will find existing `dist/*.whl` and `dist/*.tar.gz` files, relative to the location from which it is run.
* It will extract them into a temporary folder
* It will look for relative path dependencies in the metadata files that are to be replaced
* These will all be replaced by the version range that is at least equal to the current version, and still compatible.
* Then the wheel is compressed and put back in the original location

If the mono-repo is at `1.2.3`, this will modify the dependency effectively to `~1.2.3`.
Though that is a valid [SemVer range](https://devhints.io/semver), and interpretable by Poetry, it is not mentioned in [Pep-440](https://peps.python.org/pep-0440/#compatible-release).
Therefore, it will be set to `(~=1.2,>=1.2.3)`, such that it adheres to the pep standard.

**Using a Poetry plugin**

Lastly, the easiest way is to use the [poetry-plugin-mono-repo-deps](https://github.com/gerbenoostra/poetry-plugin-mono-repo-deps) Poetry plugin.
Its behavior is comparable to temporarily updating the `pyproject.toml` file, except that it only happens in memory during the run of Poetry.
 
This requires the plugin to be installed in Poetry's Python environment.
```
pipx install poetry
pipx inject poetry poetry-plugin-mono-repo-deps
```

With the plugin installed, building the artifacts is transparent:
```
cd package-b
poetry build
```

The plugin is enabled because `package-b`'s `pyproject.toml` contains the plugins section (can be empty):
```
# package-b/pyproject.toml
[tool.poetry-monorepo.deps]
```

## Adapting this to your own mono-repo
You can freely copy and use the `.sh` scripts. 
But be aware that 
* `projects.sh` contains a list of all the poetry packages, in topological order. Thus dependencies go before depending packages.
* `create_local_version.sh` and `pyproject.toml`'s `[tool.commitizen]` section contain references to all files with the repo version.

## Project structure
The root poetry project only contains development dependencies that are used on the full repo, for example, `commitizen`, and `dunamai`.

The `.gitlab-ci.yml` shows how the helper scripts can be used in a CI/CD pipeline.

As it is a mono-repo, all the packages in the repo share the same version (as defined in `VERSION`, the `pyproject.toml` and in the package's `__version__` definition) and have one shared `CHANGELOG.md`.

Each subfolder contains its own standalone `pyproject.toml` file, containing both production and development dependencies. `flake8`, `pytest` and the other dev tools are thus mentioned in each project.

The packages in the subfolder depend on each other using path dependencies, for example, `package-a = {path="../package-a", develop=true}`.

For example, we here have a [package-b/pyproject.toml](https://gitlab.com/gerbenoostra/poetry-monorepo/-/blob/main/package-b/pyproject.toml) that depends on a [package-a/pyproject.toml](https://gitlab.com/gerbenoostra/poetry-monorepo/-/blob/main/package-a/pyproject.toml) by [`path="../package-a"`](https://gitlab.com/gerbenoostra/poetry-monorepo/-/blob/main/package-b/pyproject.toml#L21).
During development, `package-b` thus depends on the local on-disk version of `package-a`.

The [service-c](https://gitlab.com/gerbenoostra/poetry-monorepo/~blob/main/package-c/) depends on `package-b`, and shows how to build a dockerfile with the wheels. 
The Dockerfile assumes `poetry_build.sh` has been run before, which will collect the wheels of all modules into `service-c`'s `/dist` folder.

## Choosing the right Python version
Poetry uses a certain Python version to create the virtual environment.

The scripts first try to set the version using `pyenv` (defined by the `.python-version` file), if that fails, it will use one of the system pythons (if available). 
One that is for example installed using `homebrew`.

## Git hooks
The repo also contains an example git hook script in [git_hook_post-checkout](https://gitlab.com/gerbenoostra/poetry-monorepo/~blob/main/git_hook_post-checkout) that you can use as `.git/hooks/post-checkout` to have git update all virtual environments to the `poetry.lock` state on every checkout.

## Further thoughts
The `dunamai` utility is also available as a [Dunamai poetry plugin](https://github.com/mtkennerly/poetry-dynamic-versioning), which can be used to set the version correctly (both for the Wheel, and in `version.py` files).
After the build, it automatically reverts the version, to keep the committed files clean.

As we can use the [poetry-plugin-mono-repo-deps](https://github.com/gerbenoostra/poetry-plugin-mono-repo-deps) Poetry plugin to build the wheels with the right version, perhaps both can be combined to build the artifacts with the correct local version.